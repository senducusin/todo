//
//  ToDoItem.swift
//  ToDo
//
//  Created by Jansen Ducusin on 5/14/21.
//

import Foundation

struct ToDoItem: Identifiable, Codable {
    var id = UUID()
    var title: String
    var important: Bool
}

extension ToDoItem {
    static let toDoItems: [ToDoItem] = [
        ToDoItem(title: "Buy Medicine", important: true),
        ToDoItem(title: "Clean Keyboard", important: false),
        ToDoItem(title: "Finish Course", important: false)
    ]
}

class ToDoStorage: ObservableObject {
    
    let userDefaultIdentifier = "toDos"
    
    @Published var toDos = [ToDoItem](){
        didSet {
            encodeAndSaveData()
        }
    }
    
    private func encodeAndSaveData(){
        do {
            let data = try PropertyListEncoder().encode(toDos)
            UserDefaults.standard.set(data, forKey: userDefaultIdentifier)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    init(){
        if let data = UserDefaults.standard.value(forKey: userDefaultIdentifier) as? Data{
            if let userDefaultToDos = try? PropertyListDecoder().decode(Array<ToDoItem>.self, from: data){
                toDos = userDefaultToDos
            }
        }
    }
    
}
