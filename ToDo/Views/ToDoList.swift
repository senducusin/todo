//
//  ToDoList.swift
//  ToDo
//
//  Created by Jansen Ducusin on 5/14/21.
//

import SwiftUI

struct ToDoList: View {
    @EnvironmentObject var toDoStorage: ToDoStorage
    
    var body: some View {
        
        NavigationView{
            List{
                ForEach(toDoStorage.toDos){ todo in
                    Text(todo.title)
                        .foregroundColor( todo.important ? .red : .blue)
                        .padding(EdgeInsets(top: 0, leading: 24, bottom: 0, trailing: 0))
                }
                .onDelete(perform: { indexSet in
                    if let index = indexSet.first {
                        toDoStorage.toDos.remove(at: index)
                    }
                })
            }
            .navigationBarTitle("To Dos")
            .navigationBarItems(trailing: NavigationLink(
                                    destination: CreateToDo(),
                                    label: {
                                        Text("Add")
                                    }))
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoList()
    }
}
