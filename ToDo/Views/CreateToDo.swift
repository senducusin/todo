//
//  CreateToDo.swift
//  ToDo
//
//  Created by Jansen Ducusin on 5/14/21.
//

import SwiftUI

struct CreateToDo: View {
    
    @State var toDoTitle: String = ""
    @State var important: Bool = false
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var toDoStorage: ToDoStorage
    
    var body: some View {
        List {
            Section {
                TextField("Title of your task", text:$toDoTitle)
            }
            
            Section{
                Toggle(isOn: $important, label: {
                    Text("Important")
                })
            }
            
            Section {
                HStack{
                    Spacer()
                    Button("Save") {
                        toDoStorage.toDos.append(ToDoItem(title: toDoTitle, important: important))
                        presentationMode.wrappedValue.dismiss()
                    }
                    Spacer()
                }
                .disabled(toDoTitle.isEmpty)
            }
        }
        .listStyle(GroupedListStyle())
    }
}

struct CreateToDo_Previews: PreviewProvider {
    static var previews: some View {
        CreateToDo().environmentObject(ToDoStorage())
    }
}
