**To Do [SwiftUI]**

A To Do app using SwiftUI.

---

## Features

- Create a task
- Display created task
- Persist tasks
- Delete a task
